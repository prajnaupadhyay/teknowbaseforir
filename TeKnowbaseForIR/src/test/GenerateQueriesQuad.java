package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import preprocess.AdjList;
import preprocess.ReadSubgraph;
import tomcat.GetPropertyValues;

public class GenerateQueriesQuad {
	
	public static void write(ArrayList<String> a, String q, String aspect, BufferedWriter bw) throws Exception
	{
		
			
		bw.write("{\n" + 
				"  \"queries\" : [\n");
		
		//bw.write(q.replace("_", " ")+" "+aspect);
		if(a!=null)
		{
			int count=1;
			for(String e:a)
			{
				if(count==1)
				{
					bw.write( 
							"    { \"number\" : \"q1\",\n" + 
							"      \"text\" : \""+q.replace("_", " ")+" "+aspect+" "+e.replace("_", " ")+"\"}\n");
					bw.write( 
							"    ,{ \"number\" : \"q1\",\n" + 
							"      \"text\" : \""+aspect+"\"}\n");

					count++;
				}
				else
				{
					bw.write(" ,{ \"number\" : \"q"+count+"\", \"text\": \""+q.replace("_", " ")+" "+aspect+" "+e.replace("_", " ")+"\"}\n");
				}
			}
		}
		bw.write("\n]}");
		
	}

	public static void main(String[] args) throws Exception
	{
		// TODO Auto-generated method stub
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		ReadSubgraph rr = new ReadSubgraph();
		AdjList aa = rr.readKB(new BufferedReader(new FileReader(hm.get("teknowbase"))));
		//System.out.println(aa.getRelTypesRelation());
		//System.out.println(aa.getRelTypes());
		BufferedReader br = new BufferedReader(new FileReader(hm.get("queries")));
		String line;
		ArrayList<String> queries = new ArrayList<String>();
		while((line=br.readLine())!=null)
		{
			queries.add(line);
		}
		
		HashMap<String, ArrayList<String>> h1 = aa.getRelTypesRelation().get("applicationof");
		HashMap<String, ArrayList<String>> h2 = aa.getRelTypesRelation().get("algorithmfor");
		HashMap<String, ArrayList<String>> h3 = aa.getRelTypesRelation().get("implementationof");
		//System.out.println("h1 is: "+h1);
		//System.out.println("h2 is: "+h2);
		//System.out.println("h3 is: "+h3);
		
		for(String q:queries)
		{
			BufferedWriter bw1 = new BufferedWriter(new FileWriter(hm.get("query_out")+q+"_application.json"));
			BufferedWriter bw2 = new BufferedWriter(new FileWriter(hm.get("query_out")+q+"_algorithm.json"));
			BufferedWriter bw3 = new BufferedWriter(new FileWriter(hm.get("query_out")+q+"_implementation.json"));
			ArrayList<String> expansion_app = h1.get(q);
			ArrayList<String> expansion_alg = h2.get(q);
			ArrayList<String> expansion_imp = h3.get(q);
			
			write(expansion_app, q, "application", bw1);
			write(expansion_alg, q, "algorithm", bw2);
			write(expansion_imp, q, "implementation", bw3);
			bw1.close();
			bw2.close();
			bw3.close();
		}
		
		
		
	}

}
