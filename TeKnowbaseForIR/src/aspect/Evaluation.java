package aspect;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.io.*;

import buildAndProcessTrees.GetPropertyValues;
import preprocess.Edge;

/**
 * class with functions related to evaluation portal
 * This code reads the database located at a remote machine. To do so, first run the following command at the system where this code will execute.
 * "ssh -p 32790 -L 3307:127.0.0.1:3306 root@10.17.50.132"
 * 		- root@10.17.50.132 is the username and IP address of the remote machine. 32790 is the port where it is hosted.
 *      
 * @author pearl
 *
 */

public class Evaluation 
{
	/**
	 * given a query, reads all papers retrieved across all heuristics and generates a hashmap of unique paper names to their abstract names
	 * @param q
	 * @param heuristiclist
	 * @param folder
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String, String> readPapersFromFolders(String q, ArrayList<String> heuristiclist, String folder) throws Exception
	{
		HashMap<String, String> paperlist = new HashMap<String, String>();
		for(String h:heuristiclist)
		{
			File f = new File(folder+"/"+h);
			for(File f1:f.listFiles())
			{
				if(f1.isDirectory())
				{
					File ff = new File(f1.getAbsolutePath()+"/"+q);
					if(ff.exists())
					{
						BufferedReader br = new BufferedReader(new FileReader(f1.getAbsolutePath()+"/"+q));
						String line;
						while((line=br.readLine())!=null)
						{
							StringTokenizer tok = new StringTokenizer(line,"\t");
							ArrayList<String> tokens = new ArrayList<String>();
							while(tok.hasMoreTokens())
							{
								tokens.add(tok.nextToken());
							}
							paperlist.put(tokens.get(1), tokens.get(2));
						}
					}
					
				}
			}
		}
		return paperlist;
	}
	
	/**
	 * reads across all heuristics and writes down all unique papers retrieved for a query into a file
	 * @param queries
	 * @param folder
	 * @param heuristics
	 * @throws Exception
	 */
	public static void getPapersForAllQueries(String queries, String folder, String heuristics) throws Exception
	{
		BufferedReader br1 = new BufferedReader(new FileReader(heuristics));
		String line1;
		ArrayList<String> heuristiclist = new ArrayList<String>();
		while((line1=br1.readLine())!=null)
		{
			heuristiclist.add(line1);
		}
		BufferedReader br = new BufferedReader(new FileReader(queries));
		String line;
		ArrayList<String> querylist = new ArrayList<String>();
		while((line=br.readLine())!=null)
		{
			querylist.add(line);
		}
		for(String q:querylist)
		{
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(folder+"/all_papers_together_top_5/"+q));
			HashMap<String, String> paperlist = readTop5PapersFromFolders(q, heuristiclist, folder);
			for(String s:paperlist.keySet())
			{
				bw.write(s+"\t"+paperlist.get(s)+"\n");
			}
			bw.close();
		}
		
	}
	
	/**
	 * same as readPapersFromFolders, but creates the list of unique papers by reading top 5 papers only
	 * @param queries
	 * @param folder
	 * @param heuristics
	 * @throws Exception
	 */
	
	public static HashMap<String, String> readTop5PapersFromFolders(String q, ArrayList<String> heuristiclist, String folder) throws Exception
	{
		HashMap<String, String> paperlist = new HashMap<String, String>();
		for(String h:heuristiclist)
		{
			File f = new File(folder+"/"+h);
			for(File f1:f.listFiles())
			{
				if(f1.isDirectory())
				{
					System.out.println(f1.getAbsolutePath());
					File ff = new File(f1.getAbsolutePath()+"/"+q);
					if(ff.exists())
					{
						BufferedReader br = new BufferedReader(new FileReader(f1.getAbsolutePath()+"/"+q));
						String line;
						int count=0;
						while((line=br.readLine())!=null)
						{
							StringTokenizer tok = new StringTokenizer(line,"\t");
							ArrayList<String> tokens = new ArrayList<String>();
							while(tok.hasMoreTokens())
							{
								tokens.add(tok.nextToken().replace("<strong>", "").replace("</strong>", ""));
							}
							count++;
							if((count>=1 && count<=5) || (count>=11 && count<=15) || (count>=21 && count<=25) || (count>=31 && count<=35) && (count>=41 && count<=45))
							{
								paperlist.put(tokens.get(1), tokens.get(2));
							}
							
						}
					}
					
				}
			}
		}
		return paperlist;
	}
	
	public static void connect(String table_name, String uname, String pwd, String dbname) throws Exception
	{
		System.out.println(table_name);
		//Class.forName("org.postgresql.Driver");
		Class.forName("com.mysql.jdbc.Driver");  
		System.out.println("Mysql JDBC Driver Registered!");
		Connection connection = null;
		connection = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3307/"+dbname, uname,
				pwd);
		
		if (connection != null) 
		{
			System.out.println("DBAdjList has been initialized");
			
		}
		else
		{
			System.out.println("Could not connect with username and password");
			System.exit(0);
		}
	}
	
	/**
	 * master function to connect to db and evaluate results
	 * @param queries
	 * @param folder
	 * @param heuristics
	 * @param table_name
	 * @param uname
	 * @param pwd
	 * @param dbname
	 * @throws Exception
	 */
	
	public static void readResults(String queries, String folder, String heuristics, String table_name, String uname, String pwd, String dbname, int k) throws Exception
	{
		System.out.println(table_name);
		//Class.forName("org.postgresql.Driver");
		//Class.forName("com.mysql.jdbc.Driver");  
		//System.out.println("Mysql JDBC Driver Registered!");
		Connection connection = null;
		//connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3307/"+dbname, uname,pwd);
		
		if (connection != null) 
		{
			System.out.println("DBAdjList has been initialized");
		}
		else
		{
			System.out.println("Could not connect with username and password");
			//System.exit(0);
		}
		BufferedReader br1 = new BufferedReader(new FileReader(heuristics));
		String line1;
		ArrayList<String> heuristiclist = new ArrayList<String>();
		while((line1=br1.readLine())!=null)
		{
			heuristiclist.add(line1);
		}
		BufferedReader br = new BufferedReader(new FileReader(queries));
		String line;
		ArrayList<String> querylist = new ArrayList<String>();
		while((line=br.readLine())!=null)
		{
			querylist.add(line);
		}
		BufferedWriter alg = new BufferedWriter(new FileWriter(folder+"/results/aspect_wise_results/algorithm"));
		BufferedWriter app = new BufferedWriter(new FileWriter(folder+"/results/aspect_wise_results/application"));
		BufferedWriter imp = new BufferedWriter(new FileWriter(folder+"/results/aspect_wise_results/implementation"));
		alg.write("query\t");
		app.write("query\t");
		imp.write("query\t");
		for(String h:heuristiclist)
		{
			alg.write(h+"\t"+h+"_dcg\t");
			app.write(h+"\t"+h+"_dcg\t");
			imp.write(h+"\t"+h+"_dcg\t");
		}
		alg.write("\n");
		app.write("\n");
		imp.write("\n");
		for(String q:querylist)
		{
			alg.write(q+"\t");
			app.write(q+"\t");
			imp.write(q+"\t");
			
			//BufferedWriter bw = new BufferedWriter(new FileWriter(folder+"/all_papers_together_top_5/"+q));
			evaluate(q, heuristiclist, folder, connection, table_name, alg, app, imp, k);
			
		}
		alg.close();
		app.close();
		imp.close();
		/*BufferedReader alg_reader = new BufferedReader(new FileReader(folder+"/results/aspect_wise_results/algorithm"));
		BufferedReader app_reader = new BufferedReader(new FileReader(folder+"/results/aspect_wise_results/application"));
		BufferedReader imp_reader = new BufferedReader(new FileReader(folder+"/results/aspect_wise_results/implementation"));
		HashMap<String, Double> scores = new HashMap<String, Double>();
		while((line=alg_reader.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			ArrayList<String> tokens = new ArrayList<String>();
			while(tok.hasMoreTokens())
			{
				tokens.add(tok.nextToken());
				
			}
			String a = tokens.get(0);
			String b = tokens.get(1);
			Double score = Double.parseDouble(tokens.get(2));
			scores.put(a+" "+b, score);
		}*/
		
	}
	
	/**
	 * writes down the results to a file
	 * @param q
	 * @param heuristiclist
	 * @param folder
	 * @param connection
	 * @param table_name
	 * @param uname
	 * @param pwd
	 * @param dbname
	 * @throws Exception
	 */
	
	public static void evaluate(String q, ArrayList<String> heuristiclist, String folder, Connection connection, String table_name, BufferedWriter alg, BufferedWriter app, BufferedWriter imp, int k) throws Exception
	{
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(folder+"/results/"+q));
		BufferedWriter bw2 = new BufferedWriter(new FileWriter(folder+"/results/"+q+"_dcg"));
		BufferedWriter bw3 = new BufferedWriter(new FileWriter(folder+"/eval_agreement.txt"));
		HashMap<String, Edge> agreement = new HashMap<String, Edge>();
		
		for(String h:heuristiclist)
		{
			File f = new File(folder+"/"+h);
			for(File f1:f.listFiles())
			{
				double sum=0;
				double sum1=0;
				///ArrayList<Double> i = query(q, f1, folder, connection, table_name, k);
				HashMap<String, Double> aspectScore = query1(q, f1, folder, connection, table_name, k,agreement);
				HashMap<String, Double> dcgScore = queryDCG(q, f1, folder, connection, table_name, k);
				if(f1.isDirectory())
				{
					 
					/*for(double ii:i)
					{
						sum = sum + ii;
					}*/
					for(String suggestion:aspectScore.keySet())
					{
						sum = sum + aspectScore.get(suggestion);
						sum1 = sum1 + dcgScore.get(suggestion);
						if(f1.getName().equals("algorithm"))
						{
							bw1.write(h+"\t"+f1.getName()+"\t"+suggestion+"\t"+aspectScore.get(suggestion)/(3)+"\n");
						}
						else if(f1.getName().equals("application"))
						{
							bw1.write(h+"\t"+f1.getName()+"\t"+suggestion+"\t"+aspectScore.get(suggestion)/(2)+"\n");
						}
						else if(f1.getName().equals("implementation"))
						{
							bw1.write(h+"\t"+f1.getName()+"\t"+suggestion+"\t"+aspectScore.get(suggestion)/(2)+"\n");
							
						}
						
						bw2.write(h+"\t"+f1.getName()+"\t"+suggestion+"\t"+dcgScore.get(suggestion)+"\n");
					}
					
					if(f1.getName().equals("algorithm"))
					{
						alg.write(sum/(aspectScore.size()*3)+"\t");
						alg.write(sum1+"\t");
					}
					else if(f1.getName().equals("application"))
					{
						app.write(sum/(aspectScore.size()*2)+"\t");
						app.write(sum1+"\t");
					}
					else if(f1.getName().equals("implementation"))
					{
						imp.write(sum/(aspectScore.size()*2)+"\t");
						imp.write(sum1+"\t");
					}
					
					
				}
				
				
			}
		}
		for(String s:agreement.keySet())
		{
			bw3.write(s+"\t"+agreement.get(s).getName()+"\t"+agreement.get(s).getRelationaName()+"\n");
		}
		bw3.close();
		alg.write("\n");
		app.write("\n");
		imp.write("\n");
		bw1.close();
		bw2.close();
		
	}
	
	/**
	 * This function returns the array of evals for each query, paper and aspect triple
	 * @param q: query
	 * @param aspect: aspect name
	 * @param heuristiclist: list of heuristic lists
	 * @param folder: root folder
	 * @param connection: mysql connection
	 * @param table_name: table where evals are stored
	 * @param uname: user name
	 * @param pwd: password
	 * @param dbname: name of db
	 * @return
	 * @throws Exception
	 */
	
	public static HashMap<String, Double> query(String q, File aspect, String folder, Connection connection, String table_name, int k) throws Exception
	{
		File ff = new File(aspect.getAbsolutePath()+"/"+q);
		ArrayList<Double> i = new ArrayList<Double>();
		HashMap<String, Double> aspectScore = new HashMap<String, Double>();
		if(ff.exists())
		{
			BufferedReader br = new BufferedReader(new FileReader(aspect.getAbsolutePath()+"/"+q));
			BufferedWriter bw = new BufferedWriter(new FileWriter(aspect.getAbsolutePath()+"/"+q+"_debug1"));
			String line;
			int count=0;
			
			while((line=br.readLine())!=null)
			{
				StringTokenizer tok = new StringTokenizer(line,"\t");
				ArrayList<String> tokens = new ArrayList<String>();
				while(tok.hasMoreTokens())
				{
					tokens.add(tok.nextToken());
				}
				System.out.println("tokens size: "+tokens.size());
				System.out.println("query and aspect: "+q+" "+aspect);
				count++;
				if((count>=1 && count<=k) || (count>=11 && count<=k+10) || (count>=21 && count<=k+20) || (count>=31 && count<=k+30) || (count>=41 && count<=k+40))
				{
					/*
					 * if(q.equals("bloom_filter") && aspect.getName().equals("algorithm") &&
					 * aspect.getAbsolutePath().contains("dbpedia")) {
					 * System.out.println(tokens.get(0)); }
					 */
					//paperlist.put(tokens.get(1), tokens.get(2));
					String suggestion = tokens.get(0);
					//System.out.println(suggestion);
					//System.out.println(tokens.get(1));
					String a = tokens.get(0).replace("<strong>", "").replace("</strong>", "");
					//String b = tokens.get(1);
					String sql1 = "select eval from "+table_name+" where query=? and paper=? and aspect=? and valid='y'";
					
					PreparedStatement statement1 = connection.prepareStatement(sql1);
					statement1.setString(1, q);
					statement1.setString(2, a);
					statement1.setString(3, aspect.getName());
					
					 bw.write(a+"\t"); if(tokens.size()==2) { bw.write(tokens.get(1)+"\t"); }
					 
					ResultSet rs1 = statement1.executeQuery();
					int val=0;
					int count1=0;
					double avg1=0;
					while(rs1.next())
					{
						
						count1++;
						val=rs1.getInt(1);
						avg1 = avg1 + val;
						bw.write(val+"\t");
						//i.add(val);
						//break;
					}
					bw.write("\n");
					
					/*if(count1==0 && q.equals("minimum_spanning_tree")) 
					{
						System.out.println(a);
						System.out.println(statement1.toString());
					}*/
					if(count1>0)
					{
						avg1 = avg1/count1;
					}
					i.add(avg1);
					if(aspectScore.get(suggestion)==null) aspectScore.put(suggestion, avg1);
					else aspectScore.put(suggestion, avg1 + aspectScore.get(suggestion));
				}
				
				
			}
			bw.close();
			br.close();
			for(String suggestion:aspectScore.keySet())
			{
				aspectScore.put(suggestion, aspectScore.get(suggestion)/k);
			}
			//System.out.println(aspectScore.size());
			
		}
		return aspectScore;
	}
	
	/**
	 * latest module to read the evals from file. Used it for CIKM and ECIR 
	 * @param q
	 * @param aspect
	 * @param folder
	 * @param connection
	 * @param table_name
	 * @param k
	 * @param agreement
	 * @return
	 * @throws Exception
	 */
	
	public static HashMap<String, Double> query1(String q, File aspect, String folder, Connection connection, String table_name, int k, HashMap<String, Edge> agreement) throws Exception
	{
		File ff = new File(aspect.getAbsolutePath()+"/"+q+"_debug1");
		ArrayList<Double> i = new ArrayList<Double>();
		HashMap<String, Double> aspectScore = new HashMap<String, Double>();
		if(ff.exists())
		{
			BufferedReader br = new BufferedReader(new FileReader(aspect.getAbsolutePath()+"/"+q+"_debug1"));
			//BufferedWriter bw = new BufferedWriter(new FileWriter(aspect.getAbsolutePath()+"/"+q+"_debug1"));
			String line;
			int count=0;
			
			while((line=br.readLine())!=null)
			{
				StringTokenizer tok = new StringTokenizer(line,"\t");
				ArrayList<String> tokens = new ArrayList<String>();
				while(tok.hasMoreTokens())
				{
					tokens.add(tok.nextToken());
				}
				System.out.println("tokens size: "+tokens.size());
				System.out.println("query and aspect: "+q+" "+aspect);
				count++;
				if((count>=1 && count<=k) || (count>=11 && count<=k+10) || (count>=21 && count<=k+20) || (count>=31 && count<=k+30) || (count>=41 && count<=k+40))
				{
					/*
					 * if(q.equals("bloom_filter") && aspect.getName().equals("algorithm") &&
					 * aspect.getAbsolutePath().contains("dbpedia")) {
					 * System.out.println(tokens.get(0)); }
					 */
					//paperlist.put(tokens.get(1), tokens.get(2));
					String suggestion = tokens.get(0);
					//System.out.println(suggestion);
					//System.out.println(tokens.get(1));
					String a = tokens.get(0).replace("<strong>", "").replace("</strong>", "");
					//String b = tokens.get(1);
					int e1=0;
					int e2=0;
					String ee1="";
					String ee2 = "";
					if(tokens.size()>3)
					{
						e1 = Integer.parseInt(tokens.get(2));
						e2 = Integer.parseInt(tokens.get(3));
						ee1 = tokens.get(2);
						ee2 = tokens.get(3);
					}
					else if(tokens.size()==3)
					{
						e1 = Integer.parseInt(tokens.get(1));
						e2 = Integer.parseInt(tokens.get(2));
						ee1 = tokens.get(1);
						ee2 = tokens.get(2);
					}
					if(agreement.get(a)==null)
					{
						agreement.put(a, new Edge(0,ee1,ee2));
					}
				//	bw.write("\n");
					
					/*if(count1==0 && q.equals("minimum_spanning_tree")) 
					{
						System.out.println(a);
						System.out.println(statement1.toString());
					}*/
					double avg1 = ((double)e1+(double) e2)/2;
					i.add(avg1);
					if(aspectScore.get("")==null) aspectScore.put("", avg1);
					else aspectScore.put("", avg1 + aspectScore.get(""));
				}
				
				
			}
		//	bw.close();
			for(String suggestion:aspectScore.keySet())
			{
				aspectScore.put(suggestion, aspectScore.get(suggestion)/k);
			}
			//System.out.println(aspectScore.size());
			
		}
		return aspectScore;
	}
	
	
	public static HashMap<String, Double> queryDCG(String q, File aspect, String folder, Connection connection, String table_name, int k) throws Exception
	{
		File ff = new File(aspect.getAbsolutePath()+"/"+q+"_debug1");
		ArrayList<Double> i = new ArrayList<Double>();
		HashMap<String, Double> aspectScore = new HashMap<String, Double>();
		if(ff.exists())
		{
			BufferedReader br = new BufferedReader(new FileReader(aspect.getAbsolutePath()+"/"+q+"_debug1"));
			//BufferedWriter bw = new BufferedWriter(new FileWriter(aspect.getAbsolutePath()+"/"+q+"_debug1"));
			String line;
			int count=0;
			
			while((line=br.readLine())!=null)
			{
				StringTokenizer tok = new StringTokenizer(line,"\t");
				ArrayList<String> tokens = new ArrayList<String>();
				while(tok.hasMoreTokens())
				{
					tokens.add(tok.nextToken());
				}
				System.out.println("tokens size: "+tokens.size());
				System.out.println("query and aspect: "+q+" "+aspect);
				count++;
				if((count>=1 && count<=k) || (count>=11 && count<=k+10) || (count>=21 && count<=k+20) || (count>=31 && count<=k+30) || (count>=41 && count<=k+40))
				{
					/*
					 * if(q.equals("bloom_filter") && aspect.getName().equals("algorithm") &&
					 * aspect.getAbsolutePath().contains("dbpedia")) {
					 * System.out.println(tokens.get(0)); }
					 */
					//paperlist.put(tokens.get(1), tokens.get(2));
					String suggestion = tokens.get(0);
					//System.out.println(suggestion);
					//System.out.println(tokens.get(1));
					String a = tokens.get(0).replace("<strong>", "").replace("</strong>", "");
					//String b = tokens.get(1);
					int e1=0;
					int e2=0;
					if(tokens.size()>=4)
					{
						e1 = Integer.parseInt(tokens.get(2));
						e2 = Integer.parseInt(tokens.get(3));
					}
					else if(tokens.size()==3)
					{
						e1 = Integer.parseInt(tokens.get(1));
						e2 = Integer.parseInt(tokens.get(2));
					}
				//	bw.write("\n");
					
					/*if(count1==0 && q.equals("minimum_spanning_tree")) 
					{
						System.out.println(a);
						System.out.println(statement1.toString());
					}*/
					double avg1 = ((double)e1+(double) e2)/2;
					i.add(avg1);
					if(aspectScore.get("")==null) aspectScore.put("", avg1/(Math.log(count+1)/Math.log(2)));
					else aspectScore.put("", avg1/(Math.log(count+1)/Math.log(2)) + aspectScore.get(""));
				}
				
				
			}
		//	bw.close();
			
			//System.out.println(aspectScore.size());
			
		}
		return aspectScore;
	}
	
	public static void readEvaluations(String folder, String heuristic, String query, String aspect, HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>> evals) throws Exception
	{
		File f = new File(folder+"/"+heuristic+"/"+aspect+"/"+query+"_debug1");
		if(f.exists())
		{
			BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath()));
			String line;
			while((line=br.readLine())!=null)
			{
				StringTokenizer tok = new StringTokenizer(line,"\t");
				ArrayList<String> tokens = new ArrayList<String>();
				while(tok.hasMoreTokens())
				{
					tokens.add(tok.nextToken());
				}
				//System.out.println(f.getAbsolutePath()+" "+tokens.size());
				if(tokens.get(0).equals(""))
				{
					tokens.remove(0);
				}
				if(evals.get(query)==null)
				{
					HashMap<String, HashMap<String, ArrayList<String>>> h = new HashMap<String, HashMap<String, ArrayList<String>>>();
					HashMap<String, ArrayList<String>> paper_eval = new HashMap<String, ArrayList<String>>();
					int c=1;
					ArrayList<String> eval_numbers = new ArrayList<String>();
					while(c<tokens.size())
					{
						if(tokens.get(c)=="0" || tokens.get(c)=="1" || tokens.get(c)=="2" || tokens.get(c)=="3")
						{
							eval_numbers.add(tokens.get(c));
						}
						c=c+1;
					}
					paper_eval.put(tokens.get(0), eval_numbers);
					h.put(aspect,paper_eval);
					evals.put(query, h);
				}
				else if(evals.get(query)!=null)
				{
					HashMap<String, HashMap<String, ArrayList<String>>> h = evals.get(query);
					if(h.get(aspect)==null)
					{
						HashMap<String, ArrayList<String>> paper_eval = new HashMap<String, ArrayList<String>>();
						int c=1;
						ArrayList<String> eval_numbers = new ArrayList<String>();
						while(c<tokens.size())
						{
							if(tokens.get(c)=="0" || tokens.get(c)=="1" || tokens.get(c)=="2" || tokens.get(c)=="3")
							{
								eval_numbers.add(tokens.get(c));
							}
							c=c+1;
						}
						paper_eval.put(tokens.get(0), eval_numbers);
						h.put(aspect,paper_eval);
					}
					else
					{
						HashMap<String, ArrayList<String>> paper_eval = h.get(aspect);
						int c=1;
						ArrayList<String> eval_numbers = new ArrayList<String>();
						while(c<tokens.size())
						{
							if(tokens.get(c).equals("0") || tokens.get(c).equals("1") || tokens.get(c).equals("2") || tokens.get(c).equals("3"))
							{
								eval_numbers.add(tokens.get(c));
							}
							c=c+1;
						}
						if(paper_eval.get(tokens.get(0))==null)
						{
							paper_eval.put(tokens.get(0), eval_numbers);
						}
						else
						{
							eval_numbers.addAll(paper_eval.get(tokens.get(0)));
							paper_eval.put(tokens.get(0), eval_numbers);
						}
						h.put(aspect, paper_eval);
					}
					evals.put(query, h);
				}
			}
		}
	}
	
	public static void updateEvaluations(String queries, String heuristics, String folder) throws Exception
	{
		BufferedReader br1 = new BufferedReader(new FileReader(heuristics));
		String line1;
		ArrayList<String> heuristiclist = new ArrayList<String>();
		while((line1=br1.readLine())!=null)
		{
			heuristiclist.add(line1);
		}
		BufferedReader br = new BufferedReader(new FileReader(queries));
		String line;
		ArrayList<String> querylist = new ArrayList<String>();
		while((line=br.readLine())!=null)
		{
			querylist.add(line);
		}
		ArrayList<String> aspectlist = new ArrayList<String>();
		aspectlist.add("algorithm");
		aspectlist.add("application");
		aspectlist.add("implementation");
		HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>> evals = new HashMap<String, HashMap<String, HashMap<String, ArrayList<String>>>>();
		for(String h:heuristiclist)
		{
			for(String a: aspectlist)
			{
				for(String q:querylist)
				{
					System.out.println(h+"\t"+a+"\t"+q);
					readEvaluations(folder, h, q, a, evals);
				}
			}
		}
		System.out.println("read evals: "+evals);
		File f1 = new File(folder+"_1");
		if(!f1.exists())
		{
			f1.mkdir();
		}
		
		for(String h:heuristiclist)
		{
			File f2 = new File(folder+"_1/"+h);
			if(!f2.exists())
			{
				f2.mkdir();
			}
			for(String a: aspectlist)
			{
				File f3 = new File(folder+"_1/"+h+"/"+a);
				if(!f3.exists())
				{
					f3.mkdir();
				}
				for(String q:querylist)
				{
					File f4 = new File(folder+"_1/"+h+"/"+a+"/"+q+"_debug1");
					System.out.println(f4.getAbsolutePath());
					BufferedWriter bw = new BufferedWriter(new FileWriter(f4.getAbsolutePath()));
						//if(evals.get(q).get(a).get(tokens.get(0)))
					File ff = new File(folder+"/"+h+"/"+a+"/"+q+"_debug1");
					File ff1 = new File(folder+"/"+h+"/"+a+"/"+q);
					File ff2=new File("");
					if(ff.exists()) ff2=ff;
					else if(ff1.exists()) ff2=ff1;
					if(ff2.exists())
					{
						BufferedReader br2 = new BufferedReader(new FileReader(ff2.getAbsolutePath()));
						while((line=br2.readLine())!=null)
						{
							StringTokenizer tok = new StringTokenizer(line,"\t");
							ArrayList<String> tokens = new ArrayList<String>();
							while(tok.hasMoreTokens())
							{
								tokens.add(tok.nextToken());
							}
							if(tokens.get(0).equals(""))
							{
								tokens.remove(0);
							}
							bw.write(tokens.get(0));
							if(tokens.size()>1)
							{
								bw.write("\t"+tokens.get(1));
							}
							else
							{
								bw.write("\t");
							}
							ArrayList<String> paper_evals = evals.get(q).get(a).get(tokens.get(0));
							if(paper_evals!=null)
							{
								for(String aa:paper_evals)
								{
									bw.write("\t"+aa);
								}
							}
							bw.write("\n");
						}
					}
					
					bw.close();
					
				}
			}
		}
	}
	
	public static void iterateAndPopulate(File folder, ArrayList<String> queries, String aspect, String table, Connection connection) throws Exception
	{
		for(String q:queries)
		{
			File f = new File(folder.getAbsolutePath()+"/"+q+"_debug1");
			if(f.exists())
			{
				BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath()));
				String line;
				while((line=br.readLine())!=null)
				{
					StringTokenizer tok = new StringTokenizer(line,"\t");
					ArrayList<String> tokens = new ArrayList<String>();
					while(tok.hasMoreTokens())
					{
						tokens.add(tok.nextToken());
					}
					int a=0;
					int b=0;
					if(tokens.size()==3)
					{
						 a = Integer.parseInt(tokens.get(1));
						 b = Integer.parseInt(tokens.get(2)); 
					}
					else if(tokens.size()>3)
					{
						 a = Integer.parseInt(tokens.get(2));
						 b = Integer.parseInt(tokens.get(3));
					}
					
					String sql1 = "insert into "+table+" values ?,?,?,?,?,?,?";
					
					PreparedStatement statement1 = connection.prepareStatement(sql1);
					statement1.setString(1, q);
					statement1.setString(2, "");
					statement1.setString(3, tokens.get(0));
					statement1.setString(4, aspect);
					statement1.setInt(3, a);
					//statement.setString
					
					// bw.write(a+"\t"); if(tokens.size()==2) { bw.write(tokens.get(1)+"\t"); }
					 
					ResultSet rs1 = statement1.executeQuery();
				}
			}
		}
	}
	
	public static void populateDatabaseFromFiles(String folder, String query, String table, String uname, String pwd, String dbname) throws Exception
	{
		System.out.println(table);
		//Class.forName("org.postgresql.Driver");
		Class.forName("com.mysql.jdbc.Driver");  
		System.out.println("Mysql JDBC Driver Registered!");
		Connection connection = null;
		connection = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3307/"+dbname, uname,
				pwd);
		
		if (connection != null) 
		{
			System.out.println("DBAdjList has been initialized");
			
		}
		else
		{
			System.out.println("Could not connect with username and password");
			System.exit(0);
		}
		
		ArrayList<String> queries = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(query));
		String line;
		while((line=br.readLine())!=null)
		{
			queries.add(line);
		}
		File f = new File(folder);
		for(File f1:f.listFiles())
		{
			if(f1.isDirectory())
			{
				File f2 = new File(f1.getAbsolutePath()+"/algorithm");
				File f3 = new File(f1.getAbsolutePath()+"/application");
				File f4 = new File(f1.getAbsolutePath()+"/implementation");
				iterateAndPopulate(f2,queries,"algorithm", table, connection);
				iterateAndPopulate(f3,queries,"application", table, connection);
				iterateAndPopulate(f4,queries,"implementation", table, connection);
			}
		}
		
	}
	
	
	public static void main(String args[]) throws Exception
	{
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		System.out.println("hiiiii");
		//updateEvaluations(hm.get("query-file"), hm.get("heuristics"), hm.get("parent-folder"));
		//connect("evaluation", "root", "admin", "tkbforir_evaluation");
		//readResults(hm.get("query-file"), hm.get("parent-folder"), hm.get("heuristics"), "evaluation", "root", "admin", "tkbforir_evaluation", Integer.parseInt(hm.get("top_k")));
		populateDatabaseFromFiles(hm.get("parent-folder"),hm.get("query-file"),"evaluation", "root", "admin", "tkbforir_evaluation");
	}
	
}
