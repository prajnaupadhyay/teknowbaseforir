package tomcat;

import java.util.*;
import java.util.Map.Entry;
import java.util.logging.*;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import com.google.common.graph.ImmutableGraph;
import com.google.common.graph.ImmutableValueGraph;
import com.google.common.graph.MutableValueGraph;

import LMForTeKnowbase.LanguageModelEntity;
import aspect.AdjListCompact;
import aspect.Aspect;
import preprocess.AdjList;
import preprocess.Edge;
import preprocess.ReadSubgraph;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
 
public class SimpleWebApp 
{
	public static HashMap<Integer, HashMap<Integer, Double>> computeProbability(String query, HashMap<String, Double> path_scores, AdjListCompact a1) throws IOException
	{
		HashMap<Integer, HashMap<Integer, Double>> prob_list = new HashMap<Integer,HashMap<Integer, Double>>();
				  
		 for(String p:path_scores.keySet()) 
		 { //System.out.println(p); 
			  StringTokenizer  tok = new StringTokenizer(p," "); 
			  ArrayList<String> tokens = new ArrayList<String>();
			  while(tok.hasMoreTokens())
			  {
				  tokens.add(tok.nextToken());
			  }
			  int ccc=0;
			  String pp="";
			  ArrayList<Integer> path1 = new ArrayList<Integer>();
			  for(String t:tokens)
			  {
				  if(ccc<tokens.size()-1)
				  {
					  if(ccc==0)
						  pp = pp+t;
					  else
						  pp = pp + " "+t;
					  path1.add((a1.relmap.get(t)));
				  }
				  ccc++;
			  }
			  
			  int source = a1.nodeMap1.get(query);
			  a1.pathConstrainedRandomWalk(path1, source, path_scores.get(p), p, prob_list); 
			  
		  } 
		 
		 return prob_list;
	}
	
	public static HashMap<String, Double> readPathScores(String file, AdjListCompact a1) throws Exception
	{
		BufferedReader br1 = new BufferedReader(new FileReader(file));
		  HashMap<Integer, HashMap<Integer, Double>> prob_list = new HashMap<Integer, HashMap<Integer, Double>>();
		  
		  HashSet<Integer> paths = new HashSet<Integer>(); 
		  HashMap<String, Double> path_scores = new HashMap<String, Double>();
		  String line;
		  while((line=br1.readLine())!=null) 
		  { 
			  StringTokenizer tok = new StringTokenizer(line," ");
			  ArrayList<String> tokens = new ArrayList<String>(); 
			  while(tok.hasMoreTokens()) 
			  {
				  tokens.add(tok.nextToken()); 
			  }
		  //System.out.println(tokens.get(0)+" "+tokens.get(1));
		  //System.out.println(tokens.get(1));
			  int ccc=0;
			  String pp="";
			  for(String t:tokens)
			  {
				  if(ccc<tokens.size()-1)
				  {
					  if(ccc==0)
						  pp=pp+t;
					  else
						  pp = pp + " "+t;
					  paths.add((a1.relmap.get(t)));
				  }
				  ccc++;
			  }
			  double score = Double.parseDouble(tokens.get(tokens.size()-1));
			  path_scores.put(pp, score);
		  
		  }
		  System.out.println("read all the meta-paths");
		  a1.getListOfNeighborsForEdgeLabel(new HashSet<Integer>(paths));
		  System.out.println("created index on all relations in meta-paths");
		  return path_scores;
	}
	
	
 
	
    public static void main(String[] args) throws Exception 
    {
		  String teknowbase_mapped="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/teknowbase_mapped.tsv";
	      String nodemap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_nodes";
	      String relmap="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/mappings_edges";
	      String wikipedia = "/mnt/dell/prajna/neo4j/input/allLinks.tsv";
	      Aspect a = new Aspect();
	      
  		AdjListCompact a1 = a.readGraphEfficientAlternate(teknowbase_mapped, relmap, nodemap);
  		String app_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/application.tsv";
  		String alg_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/algorithm.tsv";
  		String imp_meta_path="/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/implementation.tsv";
  		
  		HashMap<String, Double> app_paths = readPathScores(app_meta_path, a1);
  		HashMap<String, Double> alg_paths = readPathScores(alg_meta_path, a1);
  		HashMap<String, Double> imp_paths = readPathScores(imp_meta_path, a1);
  		//String alg_meta_path
  		
  		BufferedReader br1 = new BufferedReader(new FileReader("/mnt/dell/prajna/PreqAspect/data/galago_lookup_table.txt"));
		String line;
		HashMap<String, ArrayList<String>> supertypes = new HashMap<String, ArrayList<String>>();
		while((line=br1.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line,"\t");
			String aa = tok.nextToken();
			String bb = tok.nextToken();
			if(supertypes.get(aa)==null)
			{
				ArrayList<String> superl = new ArrayList<String>();
				superl.add(bb);
				supertypes.put(aa, superl);
			}
			else
			{
				ArrayList<String> superl = supertypes.get(aa);
				superl.add(bb);
				supertypes.put(aa, superl);
			}
			
		}
		 
  		ReadSubgraph rr = new ReadSubgraph();
  		//AdjList wiki = rr.readFromFile(new BufferedReader(new FileReader(wikipedia)));
  		MutableValueGraph<String, Integer> wiki = a.readGraphEfficientWiki(wikipedia);
  		ImmutableValueGraph<String, Integer> wiki_i = ImmutableValueGraph.copyOf(wiki);
  		System.out.println("read graph and it worked");
  		 LanguageModelEntity lm = new LanguageModelEntity();
        // String application_emb = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/embeddings/application.emd.txt";
       //  String algorithm_emb = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/embeddings/algorithm.emd.txt";
       //  String implementation_emb = "/mnt/dell/Hemanth/iitd/metapath2vec/teknowbase_upd/all_meta-paths_alternate/all_meta-paths_together/latest/embeddings/implementation.emd.txt";
        
		//HashMap<String, LanguageModelEntity> hlm_app = lm.readEmbeddingsFromFileAlternate(application_emb);
		//HashMap<String, LanguageModelEntity> hlm_alg = lm.readEmbeddingsFromFileAlternate(algorithm_emb);
		//HashMap<String, LanguageModelEntity> hlm_imp = lm.readEmbeddingsFromFileAlternate(implementation_emb);
		
		
		
		System.out.println("read embeddings");
		
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir("temp");
        tomcat.setPort(8080);
        Logger logger = Logger.getLogger("");
        Handler fileHandler = new FileHandler("/home/prajna/catalina.out", true);
        fileHandler.setFormatter(new SimpleFormatter());
        fileHandler.setLevel(Level.ALL);
        fileHandler.setEncoding("UTF-8");
        logger.addHandler(fileHandler);
        logger.log(Level.ALL, "hello");
        
        
         
        String contextPath = "/";
        String docBase = new File(".").getAbsolutePath();
         
        Context context = tomcat.addContext(contextPath, docBase);
         
        HttpServlet servlet = new HttpServlet() 
        {
            @Override
            protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                    throws ServletException, IOException 
            {
            	 PrintWriter writer = resp.getWriter();
             	writer.println("<html><title>Welcome</title><body>");
                 String query = req.getQueryString().replace("query=", "");
            	try 
				{
            	/*Class.forName("com.mysql.jdbc.Driver");  
        		System.out.println("Mysql JDBC Driver Registered!");
        		Connection connection = null;
        		connection = DriverManager.getConnection(
        				"jdbc:mysql://127.0.0.1:3307/prerequisite_aspects", "postgres",
        				"postgres");
        		
        		if (connection != null) 
        		{
        			System.out.println("Connection successful");
        		}
        		else
        		{
        			System.out.println("Could not connect with username and password");
        			System.exit(0);
        		}
               
                
               
				String sql1 = "select * from application_prob where source=? ;";
				String sql2 = "select * from algorithm_prob where source=? ;";
				String sql3 = "select * from implementation_prob where source=? ;";
				
				PreparedStatement statement1 = connection.prepareStatement(sql1);
				statement1.setString(1, query);
				ResultSet rs1 = statement1.executeQuery();
				int count=0;
				while(rs1.next())
				{
					count++;
					//break;
				}
				if(count==0)
				{
					 HashMap<Integer, HashMap<Integer, Double>> prob_list_app = computeProbability(query, app_paths, a1);
		             for(int s:prob_list_app.keySet())
		             {
		            	 for(int s1:prob_list_app.get(s).keySet())
		            	 {
		            		String sql5 = "insert into application_prob values(?,?,?) ;";
		            		PreparedStatement statement5 = connection.prepareStatement(sql5);
	     					statement5.setString(1, a1.nodeMap.get(s));
	     					statement5.setString(2, a1.nodeMap.get(s1));
	     					statement5.setDouble(3, prob_list_app.get(s).get(s1));
	     					statement5.executeQuery();
	     					
		            	 }
		             }
				}
				
				PreparedStatement statement2 = connection.prepareStatement(sql2);
				statement2.setString(1, query);
				ResultSet rs2 = statement2.executeQuery();
				int count2=0;
				while(rs2.next())
				{
					count2++;
					//break;
				}
				if(count2==0)
				{
					HashMap<Integer, HashMap<Integer, Double>> prob_list_alg = computeProbability(query, alg_paths, a1);
					 for(int s:prob_list_alg.keySet())
		             {
						 for(int s1:prob_list_alg.get(s).keySet())
		            	 {
		            		String sql5 = "insert into algorithm_prob values(?,?,?) ;";
		            		PreparedStatement statement5 = connection.prepareStatement(sql5);
	     					statement5.setString(1, a1.nodeMap.get(s));
	     					statement5.setString(2, a1.nodeMap.get(s1));
	     					statement5.setDouble(3, prob_list_alg.get(s).get(s1));
	     					statement5.executeQuery();
		            	 }
		             }
				}
				
				PreparedStatement statement3 = connection.prepareStatement(sql3);
				statement3.setString(1, query);
				ResultSet rs3 = statement3.executeQuery();
				int count3=0;
				while(rs3.next())
				{
					count3++;
					//break;
				}
				if(count3==0)
				{
	                HashMap<Integer, HashMap<Integer, Double>> prob_list_imp = computeProbability(query, imp_paths, a1);
	                for(int s:prob_list_imp.keySet())
		             {
	                	for(int s1:prob_list_imp.get(s).keySet())
		            	 {
		            		String sql5 = "insert into implementation_prob values(?,?,?) ;";
		            		PreparedStatement statement5 = connection.prepareStatement(sql5);
	     					statement5.setString(1, a1.nodeMap.get(s));
	     					statement5.setString(2, a1.nodeMap.get(s1));
	     					statement5.setDouble(3, prob_list_imp.get(s).get(s1));
	     					statement5.executeQuery();
		            	 }
		             }
				}*/
						
                LanguageModelEntity q_app=null;
                LanguageModelEntity q_alg=null;
                LanguageModelEntity q_imp=null;
                
                
               // if(hlm_app.get(query)!=null) q_app = hlm_app.get(query);
               // if(hlm_alg.get(query)!=null) q_alg = hlm_alg.get(query);
              //  if(hlm_imp.get(query)!=null) q_imp = hlm_imp.get(query);
                HashMap<String, Double> app_hm = new HashMap<String, Double>();
                HashMap<String, Double> alg_hm = new HashMap<String, Double>();
                HashMap<String, Double> imp_hm = new HashMap<String, Double>();
                
                //writer.println("<h1>Query is: "+query+", application="+hlm_app.get(query)+", algorithm="+hlm_alg.get(query)+", imp="+hlm_imp.get(query)+"</h1>");

             //   ArrayList<Edge> neigh = hm.get(query);
              /* if(q_app==null && q_alg==null && q_imp==null) 
               {
	                writer.println("<h1>Query is: "+query+", its embedding does not exists for any of the aspects. Sorry :(</h1>");
	               
	                writer.println("</body></html>");
	                //System.exit(0);
               }*/
               if(wiki.successors(query)==null)
               {
            	   	writer.println("<h1>Query is: "+query+", it is not a valid wikipedia page name. Sorry :(</h1>");
	                
	                writer.println("</body></html>");
               }
               else
               {
            	   	ArrayList<String> successors = new ArrayList<String>(wiki_i.successors(query));
					HashMap<String, Double> scores = new HashMap<String, Double>();
            	   	HashMap<String, ArrayList<String>> subtypes = new HashMap<String, ArrayList<String>>();
					for(String name:successors)
					{
            	   		if(supertypes.get(name)!=null)
            	   		{
            	   			ArrayList<String> superl = supertypes.get(name);
            	   			ArrayList<String> subtypes_names;
            	   			for(String ss:superl)
            	   			{
	            	   			if(subtypes.get(ss)!=null)
	            	   			{
	            	   				subtypes_names = subtypes.get(ss);
	            	   				subtypes_names.add(name);
	            	   				subtypes.put(ss,subtypes_names);
	            	   			}
	            	   			else
	            	   			{
	            	   				subtypes_names = new ArrayList<String>();
	            	   				subtypes_names.add(name);
	            	   				subtypes.put(ss,subtypes_names);
	            	   			}
	            	   			if(scores.get(ss)!=null)
	            	   			{
	            	   				scores.put(ss, scores.get(ss)+1);
	            	   			}
	            	   			else
	            	   			{
	            	   				scores.put(ss, 1.0);
	            	   			}
	            	   			
            	   			}
            	   		}
						/*LanguageModelEntity name_app=null;
						LanguageModelEntity name_alg=null;
						LanguageModelEntity name_imp=null;
						if(hlm_app.get(name)!=null) 
						{
							name_app=hlm_app.get(name);
							if(q_app!=null)
							{
								double cosim = q_app.cosineSimilarity(name_app);
								app_hm.put(name, cosim);
							}
						}
						if(hlm_alg.get(name)!=null) 
						{
							name_alg=hlm_alg.get(name);
							if(q_alg!=null)
							{
								double cosim = q_alg.cosineSimilarity(name_alg);
								alg_hm.put(name, cosim);
							}
						}
						if(hlm_imp.get(name)!=null) 
						{
							name_imp=hlm_imp.get(name);
							if(q_imp!=null)
							{
								double cosim = q_imp.cosineSimilarity(name_imp);
								imp_hm.put(name, cosim);
							}
						}*/
					}
					ArrayList<Entry<String, Double>> app_ordered = new ArrayList(app_hm.entrySet());
					ArrayList<Entry<String, Double>> alg_ordered = new ArrayList(alg_hm.entrySet());
					ArrayList<Entry<String, Double>> imp_ordered = new ArrayList(imp_hm.entrySet());
					
					ArrayList<Entry<String, Double>> scores_ordered = new ArrayList(scores.entrySet());
					
					Collections.sort(app_ordered, LanguageModelEntity.valueComparator);
					Collections.sort(alg_ordered, LanguageModelEntity.valueComparator);
					Collections.sort(imp_ordered, LanguageModelEntity.valueComparator);
					
					Collections.sort(scores_ordered, LanguageModelEntity.valueComparator);
					
	                writer.println("<html><title>Welcome</title><body>");
	                writer.println("<h1>Query is: "+query+"</h1>");
	               /* writer.println("<ul style=\"width:10%; float:left;\">");
	                
	                writer.println("<h3>Application aspect entities ordered are: </h3>");
	               
	                writer.println("<br>");
	                for(Entry<String, Double> e:app_ordered)
	                {
	                	writer.println("<li>"+e.getKey()+", "+e.getValue()+"</li>");
	                }
	                writer.println("</ul>");
	                writer.println("<br>");
	                writer.println("<ul style=\"width:10%; float:left;\">");
	                writer.println("<h3>Algorithm aspect entities ordered are: </h3>");
	                writer.println("<br>");
	                for(Entry<String, Double> e:alg_ordered)
	                {
	                	writer.println("<li>"+e.getKey()+", "+e.getValue()+"</li>");
	                }
	                writer.println("</ul>");
	                writer.println("<br>");
	                writer.println("<ul style=\"width:10%; float:left;\">");
	                writer.println("<h3>Implementation aspect entities ordered are: </h3>");
	                writer.println("<br>");
	                for(Entry<String, Double> e:imp_ordered)
	                {
	                	writer.println("<li>"+e.getKey()+", "+e.getValue()+"</li>");
	                }
	                writer.println("</ul>");
	                writer.println("<br>");
	                writer.println("<h3>Entities tagged are: </h3>");
	                writer.println("<br>");*/
	                for(Entry<String, Double> e:scores_ordered)
	                {
	                	writer.println("<h3>"+e.getKey()+", "+e.getValue()+"</h3>");
	                	for(String ss:subtypes.get(e.getKey()))
	                	{
	                		writer.println("<p>"+ss+"</p>");
	                	}
	                }
	                writer.println("</body></html>");
	                logger.log(Level.ALL, "wrote to file");
	                System.out.println(a1.print());
               }
			
				} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
				
                
            	//resp.setContentType("text/html");
            	// PrintWriter out = resp.getWriter();
            	// out.append("<html><title>Your string goes here</title><body>");
            	// out.append("<h1>Have a Great Day!</h1>");
               //  out.append("</body></html>");
            	// out.close();
            }
        };
         
        String servletName = "Servlet1";
        String urlPattern = "/go";
         
        tomcat.addServlet(contextPath, servletName, servlet);      
        context.addServletMappingDecoded(urlPattern, servletName);
         
        tomcat.start();
        tomcat.getServer().await();
      
    }
}